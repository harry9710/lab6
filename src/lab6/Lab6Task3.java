/*3.	Write a Java program to find all pairs of integers in an array whose sum is equal to a specified number.*/

package lab6;
import java.util.*;

public class Lab6Task3 {
	public static void main(String[] args) {
		int[] array = {3, 1, 4, 3, 2, 5, 1, 6};
		Scanner input = new Scanner(System.in);
		System.out.println("Input a number:");
		int n = input.nextInt();
		
		System.out.printf("Pairs whose sum is equal to %d:\n", n); 
        for (int i = 0; i < array.length; i++)  
        { 
            for (int j = i + 1; j < array.length; j++)  
            { 
                if (array[i] + array[j] == n)  
                    System.out.println(array[i] + " " + array[j]); 
            } 
        } 
	}
}
