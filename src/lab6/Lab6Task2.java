/*2.	Write a Java program to find the duplicates in an integer array.*/

package lab6;

public class Lab6Task2 {
	public static void main(String[] args) {
		int[] array = {3, 1, 4, 3, 2, 5, 1, 6};
		System.out.println("Duplicates:"); 
        for (int i = 0; i < array.length; i++)  
        { 
            for (int j = i + 1; j < array.length; j++)  
            { 
                if (array[i] == array[j])  
                    System.out.println(array[i]); 
            } 
        } 
	}
}
