/*1.	Create a method for adding new value to an array at the specified index. 
 * If the added value exceeds the array size, the actual array values should be copied to a new array. 
 * Remember about shifting elements. 
 * The method should take 2 input arguments: array, and new element, and return the array with added element.*/

package lab6;
import java.util.Arrays;

public class Lab6Task1 {
	public static void main(String[] args) {
		int[] arr = {3, 1, 5, 6, 2};
		System.out.println(Arrays.toString(test(arr, 1, 7))); //add number 7 to position 1 of the array
	}
	//method for adding new value to an array at the specified index
	public static int[] test(int[] array1, int position, int value) {
		//create a new array whose size is increased by 1, to store elements of the original array along with the new value
		int[] array2 = new int[array1.length + 1];	
		
		//copy elements on the left side of new value's position to new array
		for (int i = 0; i < position; i++) {
			array2[i] = array1[i];
		}
		
		array2[position] = value;
		
		//copy elements on the right side of new value's position to new array
		for (int j = position + 1; j < array2.length; j++) {
			array2[j] = array1[j-1];
		}
		
		return array2;	
	}

}
